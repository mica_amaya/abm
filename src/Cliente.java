public class Cliente {
	private String nombre;
	private String apellido;
	private String mail;
	private String usuario;
	private String contraseña;
	private int id;
	
	public Cliente(String nombre, String apellido, String mail, String usuario, String contraseña) {
		super();
		this.nombre = nombre;
		this.apellido = apellido;
		this.mail = mail;
		this.usuario = usuario;
		this.contraseña = contraseña;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getContraseña() {
		return contraseña;
	}
	public void setContraseña(String contraseña) {
		this.contraseña = contraseña;
	}
	
	

}
