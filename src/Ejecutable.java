import java.sql.Connection;
import java.sql.SQLException;

public class Ejecutable {

	public static void main(String[] args) throws SQLException, ClassNotFoundException  {
		
		BaseDeDatos bd = new BaseDeDatos();
		Cliente cliente = new Cliente("Micaela", "Amaya", "mica.amaya@gmail.com", "mica_amaya", "mica123");
		Cliente cliente2 = new Cliente("Matias", "Amaya", "matiasamaya@gmail.com", "mati_amaya", "mati_123");

		System.out.println("Nombre del cliente: " +cliente.getNombre());
		System.out.println("Nombre del cliente: " +cliente2.getNombre());
		
		bd.agregarClienteALaTabla(cliente.getNombre(), cliente.getApellido(), cliente.getMail(), cliente.getUsuario(), cliente.getContraseña());
		bd.agregarClienteALaTabla(cliente2.getNombre(), cliente2.getApellido(), cliente2.getMail(), cliente2.getUsuario(), cliente2.getContraseña());
	}

}
