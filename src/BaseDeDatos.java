import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class BaseDeDatos {
	public static void agregarClienteALaTabla(String nombre, String apellido, String mail,
			String usuario, String contraseña) throws SQLException, ClassNotFoundException {
		try {
			String myDriver = "org.gjt.mm.mysql.Driver";
			String myUrl = "jdbc:mysql://localhost/abm";
			Class.forName(myDriver);
			Connection conn = (Connection) DriverManager.getConnection(myUrl, "root", "");
			String query = "insert into cliente (nombre, apellido, mail, usuario, contraseña)"
					+ " values (?, ?, ?, ?, ?)";

			PreparedStatement preparedStmt = conn.prepareStatement(query);
			preparedStmt.setString(1, nombre);
			preparedStmt.setString(2, apellido);
			preparedStmt.setString(3, mail);
			preparedStmt.setString(4, usuario);
			preparedStmt.setString(5, contraseña);

			preparedStmt.executeUpdate();
			conn.close();
		} catch (SQLException e) {
			System.out.println("Se ha generado la siguiente excepción:");
			System.out.println(e.getMessage());
		}
	}

	public static void updateClientePorId(int id, String nombre, String apellido, String mail,
			String usuario, String contraseña) throws SQLException, ClassNotFoundException {
		try {
			String myDriver = "org.gjt.mm.mysql.Driver";
			String myUrl = "jdbc:mysql://localhost/abm";
			Class.forName(myDriver);
			Connection conn = (Connection) DriverManager.getConnection(myUrl, "root", "");
			String query = "UPDATE cliente SET nombre = ?, apellido = ?, mail = ?, usuario = ?, contraseña = ?  WHERE id = ?";
			PreparedStatement ps = conn.prepareStatement(query);
			ps.setString(1, nombre);
			ps.setString(2, apellido);
			ps.setString(3, mail);
			ps.setString(4, usuario);
			ps.setString(5, contraseña);
			ps.setInt(6, id);

			ps.executeUpdate();
			ps.close();
		} catch (SQLException | ClassNotFoundException e) {
			System.out.println("Se ha generado la siguiente excepción:");
			System.out.println(e.getMessage());

		}
	}
		
	}
	
		
	
	
	
	
	
	
	
